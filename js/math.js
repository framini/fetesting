var Mathematics = function() {
    console.log("Constructor");
};

Mathematics.prototype.add = function() {
    var counter = 0;

    Array.prototype.slice.call(arguments).forEach(function(value) {
        counter += value;
    });

    return counter;
};

Mathematics.prototype.subtract = function() {
    var index = 0,
        counter;

    Array.prototype.slice.call(arguments).forEach(function(value) {
        if (index === 0) {
            counter = value;
            index++;
        } else {
            counter -= value;
        }
    });

    return counter;
};
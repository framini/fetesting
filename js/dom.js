var DOMClass = function(options) {
    this.$el = options.el;
};

DOMClass.prototype.addClass = function(classes) {
    $(this.$el).addClass(classes);
};
var TileView = Backbone.View.extend({

    render: function() {

        this.$('.title').html(
            this.model.get('title')
        );

        this.$('.content').html(
            this.model.get('content')
        );

        return this;
    }

});
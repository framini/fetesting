/*global module:false*/
module.exports = function(grunt) {

    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // Project configuration.
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
            '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
            '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
            ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
        // Task configuration.
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true
            },
            dist: {
                src: ['js/dom.js', 'js/math.js', 'js/tilemodel.js', 'js/tileview.js', 'js/main.js'],
                dest: 'dist/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            dist: {
                src: '<%= concat.dist.dest %>',
                dest: 'dist/<%= pkg.name %>.min.js'
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            gruntfile: {
                src: 'Gruntfile.js'
            },
            lib_test: {
                src: ['js/*.js', 'test/**/*.js']
            }
        },
        casperjs: {
            options: {
                // casperjsOptions: ['--engine=slimerjs'] // Use SlimerJS (Gecko)
                // casperjsOptions: ['--log-level=debug', '--direct', '--verbose'] // Verbose logging
            },
            e2e: {
                files: {
                    'results/casper': 'test/e2e/**/*.js'
                }
            }
        },
        phantomcss: {
            desktop: {
                options: {
                    screenshots: 'test/visual-regression/desktop/',
                    results: 'results/visual-regression/desktop',
                    viewportSize: [1024, 768]
                },
                src: [
                    'test/visual-regression/**.js'
                ]
            },
            mobile: {
                options: {
                    screenshots: 'test/visual-regression/mobile/',
                    results: 'results/visual-regression/mobile',
                    viewportSize: [320, 480]
                },
                src: [
                    'test/visual-regression/**.js'
                ]
            }
        },
        karma: {
            unit: {
                configFile: 'karma.conf.js',
                background: true
            },
            continuous: {
                configFile: 'karma.conf.js',
                singleRun: true,
                browsers: ['PhantomJS'],
                reporters: ['dots']
            }
        },
        watch: {
            jshint: {
                files: ['js/*.js', 'test/unit/**/*.js'],
                tasks: ['jshint:lib_test']
            },
            karma: {
                files: ['js/*.js', 'test/unit/**/*.js'],
                tasks: ['karma:unit:run']
            }
        }
    });


    // Default task.
    grunt.registerTask('default', ['jshint', 'concat', 'uglify']);

    grunt.registerTask('devmode', ['karma:unit', 'watch']);

    grunt.registerTask('test', ['karma:continuous']);

    grunt.registerTask('teste2e', ['casperjs']);

    grunt.registerTask('testVisual', ['phantomcss']);


};
describe("Math", function() {
    var m;

    beforeEach(function() {
        m = new Mathematics();
    });

    it("should have a adding function", function() {
        expect(m.add).toBeDefined();
    });

    it("should add two or more numbers", function() {
        expect(m.add(1, 2)).toBe(3);
        expect(m.add(1, 2, 3, 4)).toBe(10);
    });

    it("should subtract two or more numbers", function() {
        expect(m.subtract(2, 1)).toBe(1);
        expect(m.subtract(4, 3, 1)).toBe(0);
    });

});
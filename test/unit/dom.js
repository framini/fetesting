describe("DOM", function() {
    var path = '',
        DOM;

    if (typeof window.__karma__ !== 'undefined') {
        path += 'base/';
    }

    jasmine.getFixtures().fixturesPath = path + 'fixture';

    beforeEach(function() {
        loadFixtures('hola.html');

        DOM = new DOMClass({
            el: '#hola'
        });
    });

    it("should have an el defined", function() {
        expect(DOM.$el).toBeDefined();
    });

    it("should have a function to add classes", function() {
        expect(DOM.addClass).toBeDefined();
    });

    it("should add classes to its asociated el", function() {
        DOM.addClass('caca');

        expect($(DOM.$el)).toHaveClass('caca');
    });

});
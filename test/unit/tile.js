describe("Tile", function() {
    var tileView;

    beforeEach(function() {
        loadFixtures("tile.html");

        tileModel = new TileModel({
            title: "Soy el titulo",
            content: "soy el contenido"
        });

        tileView = new TileView({
            model: tileModel,
            el: '.tile'
        });
    });

    it("should have an \"el\" property", function() {
        expect(tileView.el).toBeDefined();
    });

    it("should be tied to a div element", function() {
        expect(tileView.el.tagName.toLowerCase()).toBe('div');
    });

    it("should have a class of tile", function() {
        expect(tileView.$el).toHaveClass('tile');
    });

    it("should render html according to its model", function() {
        tileView.render();
        expect(tileView.el.innerHTML).toContain('<div class="title">Soy el titulo</div><div class="content">soy el contenido</div>');
    });
});
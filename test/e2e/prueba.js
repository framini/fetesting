casper.test.begin('App is setup correctly', 2, function suite(test) {
    casper.start('https://cq5kids-pub-qa.nationalgeographic.com/', function() {
        test.assertExists('header', 'Header should exist');
        test.assertExists('.menu', 'Menu should exist');
    });

    casper.run(function() {
        test.done();
    });
});
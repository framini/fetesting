module.exports = [{
    pattern: 'js/vendor/jquery-1.11.0.min.js',
    watched: false,
    served: true,
    included: true
}, {
    pattern: 'js/vendor/jasmine-jquery.js',
    watched: false,
    served: true,
    included: true
}, {
    pattern: 'js/vendor/underscore-min.js',
    watched: false,
    served: true,
    included: true
}, {
    pattern: 'js/vendor/backbone-min.js',
    watched: false,
    served: true,
    included: true
}];